## Репозиторий домашнего задания 5 блока курса MLOps-3.
# Snakemake

- В качестве проекта будем использовать данные цен на недвижимость из папки data
- Пайплайн обработки данных описан в файле [Snakefile](https://gitlab.com/Gri384/mlops5block_snakemake/-/blob/main/Snakefile?ref_type=heads)
- [Артефакты](https://gitlab.com/Gri384/mlops5block_snakemake/-/artifacts), созданные при запуске snakemake
- Wildcards используются в правилах **preprocess_data** и **train_model** в script:
- Функция expand применена в правиле **all**
- ограничение по колличеству используемых ядер указано в правиле **train_model**
``` yaml
    threads: int((len(os.sched_getaffinity(0)) + 1) / 2)
```

- Граф исполнения workflow

![Граф исполнения workflow](./workflow.svg)

- В результате обучения моделей используя 2 типа моделей и 2 видов обработанных данных получаем 4 обученные модели