# Пример скрипта обучения модели
import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from joblib import dump

smk = snakemake # type: ignore


# Чтение данных
data = pd.read_csv(smk.input[0])

features = ['SqFt','Bedrooms','Bathrooms','Offers']

y = data.Price
X = data[features]

model = DecisionTreeRegressor(random_state=1)
model.fit(X, y)

# Сохранение модели
dump(model, smk.output[0])
