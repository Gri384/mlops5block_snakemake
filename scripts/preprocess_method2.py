# Пример скрипта предобработки
import pandas as pd

smk = snakemake # type: ignore

# Чтение данных
data = pd.read_csv(
    smk.input[0]
)

data = data.dropna(axis=0)

# Сохранение обработанных данных
data.to_csv(smk.output[0], index=False)
