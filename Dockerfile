FROM mambaorg/micromamba

WORKDIR /app 
COPY env.yml /app/env.yml

# Создание и активация окружения для разработки машинного обучения
RUN micromamba create -f env.yml
